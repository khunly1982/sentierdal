﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Coordonnee
    {
        public Coordonnee()
        {
            Plantes = new HashSet<Plante>();
        }

        public int Id { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public double? Alt { get; set; }

        public virtual ICollection<Plante> Plantes { get; set; }
    }
}
