﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Insecte
    {
        public Insecte()
        {
            PlanteInsectes = new HashSet<PlanteInsecte>();
            TraductionInsectes = new HashSet<TraductionInsecte>();
        }

        public int Id { get; set; }
        public string Icone { get; set; }

        public virtual ICollection<PlanteInsecte> PlanteInsectes { get; set; }
        public virtual ICollection<TraductionInsecte> TraductionInsectes { get; set; }
    }
}
