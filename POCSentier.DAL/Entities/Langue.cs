﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Langue
    {
        public Langue()
        {
            TraductionInsectes = new HashSet<TraductionInsecte>();
            TraductionPlantes = new HashSet<TraductionPlante>();
            TraductionSaisons = new HashSet<TraductionSaison>();
            TraductionStatuts = new HashSet<TraductionStatut>();
            TraductionVertus = new HashSet<TraductionVertu>();
        }

        public int IdLangue { get; set; }
        public string Nom { get; set; }
        public string CodeIso { get; set; }

        public virtual ICollection<TraductionInsecte> TraductionInsectes { get; set; }
        public virtual ICollection<TraductionPlante> TraductionPlantes { get; set; }
        public virtual ICollection<TraductionSaison> TraductionSaisons { get; set; }
        public virtual ICollection<TraductionStatut> TraductionStatuts { get; set; }
        public virtual ICollection<TraductionVertu> TraductionVertus { get; set; }
    }
}
