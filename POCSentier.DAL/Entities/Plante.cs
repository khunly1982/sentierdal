﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Plante
    {
        public Plante()
        {
            PlanteInsectes = new HashSet<PlanteInsecte>();
            PlanteSaisons = new HashSet<PlanteSaison>();
            TraductionPlantes = new HashSet<TraductionPlante>();
        }

        public int Id { get; set; }
        public int CoordonneeId { get; set; }

        public virtual Coordonnee Coordonnee { get; set; }
        public virtual PlanteInitie PlanteInitie { get; set; }
        public virtual ICollection<PlanteInsecte> PlanteInsectes { get; set; }
        public virtual ICollection<PlanteSaison> PlanteSaisons { get; set; }
        public virtual ICollection<TraductionPlante> TraductionPlantes { get; set; }
    }
}
