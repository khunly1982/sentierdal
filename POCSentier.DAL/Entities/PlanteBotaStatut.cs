﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class PlanteBotaStatut
    {
        public int PlanteId { get; set; }
        public int StatutId { get; set; }

        public virtual PlanteBotum Plante { get; set; }
        public virtual Statut Statut { get; set; }
    }
}
