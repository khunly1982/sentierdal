﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class PlanteBotum
    {
        public PlanteBotum()
        {
            PlanteBotaStatuts = new HashSet<PlanteBotaStatut>();
        }

        public int PlanteId { get; set; }

        public virtual PlanteInitie Plante { get; set; }
        public virtual ICollection<PlanteBotaStatut> PlanteBotaStatuts { get; set; }
    }
}
