﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class PlanteInitie
    {
        public PlanteInitie()
        {
            PlanteInitieVertus = new HashSet<PlanteInitieVertu>();
        }

        public int PlanteId { get; set; }
        public string NomLatin { get; set; }

        public virtual Plante Plante { get; set; }
        public virtual PlanteBotum PlanteBotum { get; set; }
        public virtual ICollection<PlanteInitieVertu> PlanteInitieVertus { get; set; }
    }
}
