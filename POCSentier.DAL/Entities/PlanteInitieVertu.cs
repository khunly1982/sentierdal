﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class PlanteInitieVertu
    {
        public int PlanteId { get; set; }
        public int VertuId { get; set; }

        public virtual PlanteInitie Plante { get; set; }
        public virtual Vertu Vertu { get; set; }
    }
}
