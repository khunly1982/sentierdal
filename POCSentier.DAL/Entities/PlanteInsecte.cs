﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class PlanteInsecte
    {
        public int Id { get; set; }
        public int PlanteId { get; set; }
        public int InsecteId { get; set; }
        public string Type { get; set; }

        public virtual Insecte Insecte { get; set; }
        public virtual Plante Plante { get; set; }
    }
}
