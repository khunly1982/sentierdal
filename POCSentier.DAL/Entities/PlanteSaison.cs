﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class PlanteSaison
    {
        public int PlanteId { get; set; }
        public int SaisonId { get; set; }

        public virtual Plante Plante { get; set; }
        public virtual Saison Saison { get; set; }
    }
}
