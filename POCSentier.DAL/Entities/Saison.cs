﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Saison
    {
        public Saison()
        {
            PlanteSaisons = new HashSet<PlanteSaison>();
            TraductionSaisons = new HashSet<TraductionSaison>();
        }

        public int Id { get; set; }

        public virtual ICollection<PlanteSaison> PlanteSaisons { get; set; }
        public virtual ICollection<TraductionSaison> TraductionSaisons { get; set; }
    }
}
