﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Statut
    {
        public Statut()
        {
            PlanteBotaStatuts = new HashSet<PlanteBotaStatut>();
            TraductionStatuts = new HashSet<TraductionStatut>();
        }

        public int Id { get; set; }

        public virtual ICollection<PlanteBotaStatut> PlanteBotaStatuts { get; set; }
        public virtual ICollection<TraductionStatut> TraductionStatuts { get; set; }
    }
}
