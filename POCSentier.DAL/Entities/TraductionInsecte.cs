﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class TraductionInsecte
    {
        public int IdInsecte { get; set; }
        public int IdLangue { get; set; }
        public string Nom { get; set; }

        public virtual Insecte IdInsecteNavigation { get; set; }
        public virtual Langue IdLangueNavigation { get; set; }
    }
}
