﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class TraductionPlante
    {
        public int IdPlante { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int IdLangue { get; set; }

        public virtual Langue IdLangueNavigation { get; set; }
        public virtual Plante IdPlanteNavigation { get; set; }
    }
}
