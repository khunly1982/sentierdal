﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class TraductionSaison
    {
        public int IdSaison { get; set; }
        public int IdLangue { get; set; }
        public string Nom { get; set; }

        public virtual Langue IdLangueNavigation { get; set; }
        public virtual Saison IdSaisonNavigation { get; set; }
    }
}
