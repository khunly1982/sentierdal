﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class TraductionStatut
    {
        public int IdStatut { get; set; }
        public int IdLangue { get; set; }
        public string Nom { get; set; }

        public virtual Langue IdLangueNavigation { get; set; }
        public virtual Statut IdStatutNavigation { get; set; }
    }
}
