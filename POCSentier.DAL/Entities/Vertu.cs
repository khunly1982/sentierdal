﻿using System;
using System.Collections.Generic;

#nullable disable

namespace POCSentier.DAL.Entities
{
    public partial class Vertu
    {
        public Vertu()
        {
            PlanteInitieVertus = new HashSet<PlanteInitieVertu>();
            TraductionVertus = new HashSet<TraductionVertu>();
        }

        public int Id { get; set; }

        public virtual ICollection<PlanteInitieVertu> PlanteInitieVertus { get; set; }
        public virtual ICollection<TraductionVertu> TraductionVertus { get; set; }
    }
}
