﻿using System.Data.SqlClient;
using POCSentier.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCSentier.DAL.Services
{
    public class InsecteService
    {
        private SqlConnection oConn;

        public InsecteService(SqlConnection oConn)
        {
            this.oConn = oConn;
        }

        public List<Insecte> Get()
        {
            try
            {
                oConn.Open();
                string requete = "SELECT Id, Icone FROM Insecte";
                SqlCommand cmd = oConn.CreateCommand();
                cmd.CommandText = requete;
                SqlDataReader reader = cmd.ExecuteReader();
                List<Insecte> result = new List<Insecte>();
                while(reader.Read())
                {
                    result.Add(new Insecte {
                        Id = (int)reader["Id"],
                        Icone = (string)reader["Icone"]
                    });
                }
                return result;
            }
            catch(Exception e)
            {
                throw;
            }
            finally
            {
                oConn.Close();
            }
        }

        public int Add(Insecte insecte)
        {
            try
            {
                oConn.Open();
                SqlCommand cmd = oConn.CreateCommand();
                cmd.CommandText = "INSERT INTO Insecte(Icone) OUTPUT inserted.Id VALUES (@p1)";
                cmd.Parameters.AddWithValue("p1", insecte.Icone);
                return (int)cmd.ExecuteScalar();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oConn.Close();
            }
        }

        public bool Update(Insecte insecte)
        {
            // UPDATE ... SET ... = ... WHERE ... = ...
            try
            {
                oConn.Open();
                SqlCommand cmd = oConn.CreateCommand();
                cmd.CommandText = "UPDATE Insecte SET Icone = @p1 WHERE Id = @p2";
                cmd.Parameters.AddWithValue("p1", insecte.Icone);
                cmd.Parameters.AddWithValue("p2", insecte.Id);
                return cmd.ExecuteNonQuery() != 0;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oConn.Close();
            }
        }

        public bool Delete(int id)
        {
            // DELETE FROM ... WHERE ... = ....

            try
            {
                oConn.Open();
                SqlCommand cmd = oConn.CreateCommand();
                cmd.CommandText = "DELETE FROM Insecte WHERE Id = @p1";
                cmd.Parameters.AddWithValue("p1", id);
                return cmd.ExecuteNonQuery() != 0;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oConn.Close();
            }
        }

        public int AddWithTranslations(Insecte insecte)
        {
            SqlTransaction t = null;
            try
            {
                // ouvrir la connection
                oConn.Open();
                // creer une transaction
                t = oConn.BeginTransaction();
                SqlCommand cmd1 = oConn.CreateCommand();
                cmd1.Transaction = t;
                cmd1.CommandText
                    = "INSERT INTO Insecte OUTPUT inserted.Id VALUES (@p1)";
                cmd1.Parameters.AddWithValue("p1", insecte.Icone);
                int id = (int)cmd1.ExecuteScalar();
                foreach(TraductionInsecte trad in insecte.TraductionInsectes)
                {
                    SqlCommand cmd = oConn.CreateCommand();
                    cmd.Transaction = t;
                    cmd.CommandText
                        = @"INSERT INTO TraductionInsecte(IdInsecte, IdLangue, Nom) 
                            VALUES (@p1, @p2, @p3)";
                    cmd.Parameters.AddWithValue("p1", id);
                    cmd.Parameters.AddWithValue("p2", trad.IdLangue);
                    cmd.Parameters.AddWithValue("p3", trad.Nom);
                    cmd.ExecuteNonQuery();
                }
                t.Commit();
                return id;
            }
            catch(Exception e)
            {
                t?.Rollback();
                throw;
            }
            finally
            {
                // fermer la connection
                oConn.Close();
            }
            
        }
    }
}
