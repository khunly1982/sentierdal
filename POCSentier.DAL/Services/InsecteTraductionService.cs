﻿using POCSentier.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCSentier.DAL.Services
{
    public class InsecteTraductionService
    {
        private SqlConnection oConn;

        public InsecteTraductionService(SqlConnection oConn)
        {
            this.oConn = oConn;
        }

        public List<TraductionInsecte> Get()
        {
            try
            {
                oConn.Open();
                SqlCommand cmd = oConn.CreateCommand();
                cmd.CommandText = "SELECT IdLangue, IdInsecte, Nom FROM TraductionInsecte";
                SqlDataReader reader = cmd.ExecuteReader();
                List<TraductionInsecte> result = new List<TraductionInsecte>();
                while(reader.Read())
                {
                    result.Add(new TraductionInsecte
                    {
                        IdInsecte = (int)reader["IdInsecte"],
                        IdLangue = (int)reader["IdLangue"],
                        Nom = (string)reader["Nom"],
                    });
                }
                return result;
            }
            catch (Exception e)
            {

                throw;
            }
            finally
            {
                oConn.Close();
            }
        }
    }
}
