﻿using POCSentier.DAL.Entities;
using POCSentier.DAL.Services;
using System;
using System.Data.SqlClient;

namespace POCSentier
{
    class Program
    {
        static void Main(string[] args)
        {

            // Me connecter
                // - LE chemin vers le serveur
                // - Le nom de la DB
                // - Les credentials
                // ==> ConnectionString
            string ConnectionString = @"Data Source=K-LAPTOP;Initial Catalog=Sentier;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            // Il me faut un objet qui va permettre la connexion sql
            // ==> SqlConnection
            //using (SqlConnection oConn = new SqlConnection(ConnectionString))
            //{
            //    // Ouvrir la connection
            //    try
            //    {
            //        oConn.Open();

            //        // Ecrire ma requête sql
            //        string requete = @"SELECT TraductionVertu.Nom, CodeIso
            //                            FROM TraductionVertu
            //                            JOIN Langues
            //                            ON TraductionVertu.IdLangue = Langues.IdLangue
            //                            WHERE
            //                            CodeIso = 'FR-BE'
            //                            ORDER BY Nom ASC";
            //        // Exécuter ma requête sql
            //        // J'ai besoin d'un objet qui va véhiculer ma requete vers le serveur via la connexion
            //        //==>SqlCommand
            //        SqlCommand oCmd = new SqlCommand(requete, oConn); //J'ai mis les gosse dans la voiture
            //        //JE dois démarrer==> Exécuter ma requete
            //        SqlDataReader oDr = oCmd.ExecuteReader();
            //        while (oDr.Read())
            //        {
            //            // Parcourir le résultat de ma requête sql
            //            Console.WriteLine($"{oDr["Nom"]} - {oDr["CodeIso"]}");
            //        }
            //        //CE que j'ouvre je FERME!!!
            //        oDr.Close();
            //    }
            //    catch (Exception ex)
            //    {

            //        throw;
            //    }
            //    finally
            //    {
            //        //Si ma connection n'est pas fermée, je la FERME!!!!!
            //        if (oConn.State != System.Data.ConnectionState.Closed)
            //        {
            //            try
            //            {
            //                oConn.Close();
            //            }
            //            catch (Exception ex)
            //            {

            //                throw;
            //            }
            //        }
            //    }

            //}

            // creer une connection
            //using (SqlConnection oConn = new SqlConnection(ConnectionString))
            //{
            //    try
            //    {
            //        // ouvrir la connection
            //        oConn.Open();
            //        // definir la requete 
            //        string requete1 = "INSERT INTO Vertu VALUES(5)";
            //        string requete2 = "INSERT INTO TraductionVertu(IdLangue, IdVertu, Nom) VALUES (@p1, @p2, @p3)";
            //        // Creer un commande
            //        SqlCommand cmd1 = new SqlCommand(requete1, oConn);
            //        cmd1.ExecuteNonQuery();

            //        SqlCommand cmd = new SqlCommand(requete2, oConn);
            //        cmd.Parameters.AddWithValue("p1", 1);
            //        cmd.Parameters.AddWithValue("p2", 5);
            //        cmd.Parameters.AddWithValue("p3", "hydratation");
            //        cmd.ExecuteNonQuery();

            //        // executer la commande
            //    } catch (Exception e)
            //    {
            //    } finally {
            //        // fermer la connection 
            //        oConn.Close();
            //    }
            //}
            using (SqlConnection oConn = new SqlConnection(ConnectionString))
            {
                InsecteService service = new InsecteService(oConn);

                Insecte i = new Insecte { Icone = "test machin" };
                i.TraductionInsectes.Add(new TraductionInsecte { 
                    IdLangue = 1,
                    Nom = "Scarabee"
                });
                i.TraductionInsectes.Add(new TraductionInsecte
                {
                    IdLangue = 2,
                    Nom = "Beetle"
                });
                i.TraductionInsectes.Add(new TraductionInsecte
                {
                    IdLangue = 3,
                    Nom = "Kever"
                });

                service.AddWithTranslations(i);

                //if(service.Update(new Insecte { Id = 4, Icone = "kjfsdjfj" }))
                //{
                //    Console.WriteLine("ok");
                //}
                //else
                //{
                //    Console.WriteLine("KO");
                //}
                //foreach (Insecte item in servive.Get())
                //{
                //    Console.WriteLine(item.Id);
                //    Console.WriteLine(item.Icone);
                //}

                //InsecteService s = new InsecteService(oConn);
                //int id = s.Add(new Insecte { Icone = "https://lemagdesanimaux.ouest-france.fr/images/dossiers/2020-03/hanneton-095443.jpg" });
                //Console.WriteLine(id);
            }


        }
    }
}
